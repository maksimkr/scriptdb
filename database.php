<?php
class DataBase
{
	private $config;
	private $pdo;
	
	public function __construct($config = null)
	{
		if($config)
		{
			$dsn = "mysql:host=" . $config['host'] . ";dbname=" . $config['database'] . ";charset=utf8";
			$this->pdo = new PDO($dsn, $config['user'] , $config['password']);
			$this->pdo->setAttribute( PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC ); 
			$this->pdo->setAttribute( PDO::ATTR_PERSISTENT, true );
			
		}
	}
	
	public function query($sql = ''){
		return $this->pdo->query($sql);
	}
	
	public function ContactFormQuery($name, $email, $message)
	{
		try {
			$stmt = $this->pdo->prepare("INSERT INTO `ContactFormMessages` (`name`,`email`,`message`) VALUES (?,?,?)");
			$stmt -> execute(array($name,$email,$message));
		}
		catch(PDOException $e){
			echo 'Error : '.$e->getMessage();
			exit();
		}
	}
	
	public function ContactFormRemove($id){
		try {
			$stmt = $this->pdo->prepare("DELETE FROM `ContactFormMessages` WHERE `id` = ?");
			$stmt -> execute(array($id));
		}
		catch(PDOException $e){
			echo 'Error : '.$e->getMessage();
			exit();
		}
	}
}